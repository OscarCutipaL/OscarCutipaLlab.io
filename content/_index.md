---
title: Home
name: Oscar Cutipa Luque
date: 2017-02-20T15:26:23-06:00

categories: [one]
tags: [two,three,four]

interestslabel: "Mis intereses"
interests: [
    "Spatio-Temporal Modelling",
    "Bayesian Statistics",
    "Computational Intensive Methods",
    "Causal Modelling"
  ]
applicationslabel: Aplicaciones
applications: [
    "Epidemiology",
    "Climate Change",
    "Extreme Events",
    "Criminology",
    "Others Complex Systems"
  ]
educationlabel: Educación
education:
  second:
    course: MSc in Statistics
    university:  "[Lancaster University](https://www.lancaster.ac.uk/)"
    years: 2015 - 2016
  third:
    course: BSc in Engineering Statistics
    university: "[Universidad Nacional de Ingeniería](https://www.uni.edu.pe)"
    years: 2019 - 2024
---


Estudiante de 9no ciclo de Ingeniería Estadística de la Universidad Nacional de Ingeniería, perteneciente al décimo superior, apasionado por la estadística, por conocer nuevas ramas
y dispuesto a afrontar nuevos desafíos en la investigación.
Soy un alumno con fuertes deseos de contribuir conocimiento
en el campo de la estadística. Además he estado en diferentes grupos de la facultad, lo cual me ha permitido desarrollar el trabajo el equip
 I use open
source software to implement my work such as Julia, R and Python. Check my
[GitHub](https://github.com/ErickChacon) and [GitLab](https://gitlab.com/ErickChacon)
accounts to know more about my current work.
